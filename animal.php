<?php

class Animal
{
  public $name;
  public $legs = 2;
  public $cold_blooded = "FALSE";

  function __construct($nama)
  {
    $this->name = $nama;
  }
  function getName()
  {
    return $this->name;
  }
  function getLegs()
  {

    return $this->legs;
  }
  function getCold_blooded()
  {
    return $this->cold_blooded;
  }
}

// class Ape extends Animal
// {
//   function get_yell()
//   {
//     echo "Auooo";
//   }
// }

// class Frog extends Animal
// {
//   public $legs = 4;

//   function get_jump()
//   {
//     echo "Hop - hop";
//   }
// }
