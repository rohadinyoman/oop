<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  require 'animal.php';
  require 'frog.php';
  require 'ape.php';

  $sheep = new Animal("Shaun");

  echo $sheep->getName() . " ";
  echo $sheep->getLegs() . " ";
  echo $sheep->getCold_blooded() . "<br><br>";

  $sungokong = new Ape("Kera Sakti");
  echo $sungokong->getName() . " ";
  echo $sungokong->getLegs() . " ";
  echo $sungokong->get_yell() . "<br><br>";

  $kodok = new Frog("buduk");
  echo $kodok->getName() . " ";
  echo $kodok->getLegs() . " ";
  echo $kodok->get_jump();
  ?>

</body>

</html>